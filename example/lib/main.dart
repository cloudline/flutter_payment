import 'package:flutter/material.dart';
import 'package:flutter_payment/flutter_payment.dart';

import 'add_cc.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool walletAvailable;

  @override
  void initState() {
    walletAvailable = false;
    FlutterPayment.isWalletAvailable().then((available) {
      setState(() {
        walletAvailable = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Builder(
          builder: (context) {
            return Container(
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  !walletAvailable
                      ? Container()
                      : RaisedButton(
                          onPressed: () async {
                            await FlutterPayment.payWithWallet();
                          },
                          child: Text('Pay with Android Pay'),
                        ),
                  RaisedButton(
                    child: Text('Insert CC'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AddCC(),
                          fullscreenDialog: true,
                        ),
                      );
                    },
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
