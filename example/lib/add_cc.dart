import 'package:flutter/material.dart';
import 'package:flutter_payment/flutter_payment.dart';
import 'package:flutter_payment/material.dart';

class AddCC extends StatelessWidget {
  final _key = GlobalKey<CardFormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add your credit card'),
        actions: <Widget>[
          FlatButton(
            onPressed: () async {
              if (_key.currentState.validate()) {
                final token =
                    await FlutterPayment.getCardToken(_key.currentState.card);
                debugPrint(token);
                Navigator.of(context).pop();
              }
            },
            child: Text(
              'SAVE',
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: CardForm(
          key: _key,
          zipCode: true,
        ),
      ),
    );
  }
}
