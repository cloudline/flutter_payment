# flutter_payment

A Stripe plugin for Flutter

## Android configuration
Add your publishable key to your strings.xml file.
```xml
<resources>
    <string name="publishable_key">pk_test_123</string>
</resources>
```
Add this two lines to the app/gradle file
```
defaultConfig {
  ...
  multiDexEnabled true
}

dependencies {
  ...
  implementation 'androidx.multidex:multidex:2.0.1'
}
```

## iOS configuration
Create a Stripe.plist file in the runner directory with your publishable key
```xml
<plist version="1.0">
<dict>
	<key>PublishableKey</key>
	<string>pk_test_123</string>
</dict>
</plist>
```

## Using the CardForm
The plugin brings a Credit Card form. The form comes in both flavors, material and cupertino.

### Material form
```dart
import 'package:flutter_payment/flutter_payment.dart';
import 'package:flutter_payment/material.dart';

class MyWidget extends StatelessWidget {
  final _key = GlobalKey<CardFormState>();
  
  @override
  Widget build() {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add your credit card"),
        actions: <Widget>[
          FlatButton(
            onPressed: () async {
              if (_key.currentState.validate()) {
                final token = await FlutterPayment.getCardToken(
                    _key.currentState.card);
              }
            },
            child: Text('SAVE'),
          )
        ],
      ),
      body: CardForm(key: _cardFormKey)
    );
  }
}
```
