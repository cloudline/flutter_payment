package com.cloudlinesolutions.flutter_payment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.stripe.android.GooglePayConfig;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * FlutterPaymentPlugin
 */
public class FlutterPaymentPlugin implements MethodCallHandler, PluginRegistry.ActivityResultListener {
    private final Activity currentActivity;
    private final int LOAD_PAYMENT_DATA_REQUEST_CODE = 52;
    private Result result;

    private FlutterPaymentPlugin(Activity activity) {
        currentActivity = activity;
    }

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_payment");
        FlutterPaymentPlugin instance = new FlutterPaymentPlugin(registrar.activity());

        channel.setMethodCallHandler(instance);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull final Result result) {
        this.result = result;
        String method = call.method;
        Card card;

        final PaymentsClient paymentsClient = Wallet.getPaymentsClient(currentActivity,
                new Wallet.WalletOptions.Builder()
                        .setEnvironment(WalletConstants.ENVIRONMENT_TEST)
                        .build());

        final IsReadyToPayRequest request = IsReadyToPayRequest.newBuilder()
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                .build();

        String publishableString = getResourceStringFromContext(currentActivity, "publishable_key");

        switch (method) {
            case "getPlatformVersion":
                result.success("Android " + android.os.Build.VERSION.RELEASE);
                break;
            case "isWalletAvailable":
                paymentsClient.isReadyToPay(request).addOnCompleteListener(
                        new OnCompleteListener<Boolean>() {
                            public void onComplete(Task<Boolean> task) {
                                try {
                                    final boolean taskResult =
                                            task.getResult(ApiException.class);
                                    result.success(taskResult);
                                } catch (ApiException exception) {
                                    result.error("NOT_READY_FOR_WALLER", exception.getMessage(), null);
                                }
                            }
                        }
                );

                break;
            case "payWithWallet":
                try {
                    PaymentConfiguration.init(publishableString);
                    AutoResolveHelper.resolveTask(
                            paymentsClient.loadPaymentData(createPaymentDataRequest()),
                            currentActivity,
                            LOAD_PAYMENT_DATA_REQUEST_CODE
                    );
                } catch (JSONException e) {
                    result.error("JSON_ERROR", e.getMessage(), null);
                }
                break;
            case "validateCard":
                card = cardFromMethodCall(call);
                result.success(card.validateCard());
                break;
            case "getCreditCardToken":
                card = cardFromMethodCall(call);
                if (!card.validateCard()) {
                    result.error("INVALID_CREDIT_CARD_VALUES", "Error validating card", null);
                    return;
                }

                if (publishableString.trim().isEmpty()) {
                    result.error("INVALID_PUBLISHABLE_KEY", "Missing publishable key", null);
                }

                new Stripe(currentActivity).createToken(card, publishableString, new TokenCallback() {
                    @Override
                    public void onSuccess(@NonNull Token token) {
                        result.success(token.getId());
                    }

                    @Override
                    public void onError(@NonNull Exception e) {
                        result.error("INVALID_TOKEN", e.getMessage(), null);
                    }
                });
                break;
            default:
                result.notImplemented();
                break;
        }
    }

    @NonNull
    private PaymentDataRequest createPaymentDataRequest() throws JSONException {
        final JSONObject tokenizationSpec =
                new GooglePayConfig().getTokenizationSpecification();

        final JSONObject cardPaymentMethod = new JSONObject()
                .put("type", "CARD")
                .put(
                        "parameters",
                        new JSONObject()
                                .put("allowedAuthMethods", new JSONArray()
                                        .put("PAN_ONLY")
                                        .put("CRYPTOGRAM_3DS"))
                                .put("allowedCardNetworks",
                                        new JSONArray()
                                                .put("AMEX")
                                                .put("DISCOVER")
                                                .put("JCB")
                                                .put("MASTERCARD")
                                                .put("VISA"))

                                // require billing address
                                .put("billingAddressRequired", true)
                                .put(
                                        "billingAddressParameters",
                                        new JSONObject()
                                                .put("format", "FULL")
                                                .put("phoneNumberRequired", true)
                                )
                )
                .put("tokenizationSpecification", tokenizationSpec);

        // create PaymentDataRequest
        final JSONObject paymentDataRequest = new JSONObject()
                .put("apiVersion", 2)
                .put("apiVersionMinor", 0)
                .put("allowedPaymentMethods",
                        new JSONArray().put(cardPaymentMethod))
                .put("transactionInfo", new JSONObject()
                        .put("totalPrice", "10.00")
                        .put("totalPriceStatus", "FINAL")
                        .put("currencyCode", "USD")
                )
                .put("merchantInfo", new JSONObject()
                        .put("merchantName", "Example Merchant"))

                // require email address
                .put("emailRequired", true);

        return PaymentDataRequest.fromJson(paymentDataRequest.toString());
    }

    private Card cardFromMethodCall(MethodCall call) {
        String cardNumber = call.argument("number");
        int cardExpMonth = call.argument("expMonth");
        int cardExpYear = call.argument("expYear");
        String cardCVC = call.argument("cvc");

        String name = call.argument("name");
        String addressZip = call.argument("addressZip");
        String addressLine1 = call.argument("addressLine1");
        String addressLine2 = call.argument("addressLine2");
        String addressCity = call.argument("addressCity");
        String addressCountry = call.argument("addressCountry");
        String addressState = call.argument("addressState");

        return Card.create(cardNumber, cardExpMonth, cardExpYear, cardCVC)
                .toBuilder()
                .name(name)
                .addressZip(addressZip)
                .addressLine1(addressLine1)
                .addressLine2(addressLine2)
                .addressCity(addressCity)
                .addressCountry(addressCountry)
                .addressState(addressState)
                .build();
    }

    private static String getResourceStringFromContext(Context context, String resName) {
        final int stringRes = context.getResources().getIdentifier(resName, "string", context.getPackageName());
        if (stringRes == 0) {
            throw new IllegalArgumentException(String.format("The 'R.string.%s' value it's not defined in your project's resources file.", resName));
        }
        return context.getString(stringRes);
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOAD_PAYMENT_DATA_REQUEST_CODE) {
            if (resultCode == currentActivity.RESULT_OK) {
                this.result.success("Payment done!");
                return true;
            }
        }

        this.result.error("WALLER_PAYMENT_ERROR", "Couldn't make the payment with Google Pay", null);
        return false;
    }
}
