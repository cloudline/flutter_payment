import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CreditCardFormatter extends TextInputFormatter {
  final String separator;

  CreditCardFormatter({
    @required this.separator,
  }) {
    assert(separator != null);
  }

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length > 0) {
      String clean = newValue.text.replaceAll(new RegExp(separator), '');
      if (clean.length > 16) {
        clean = clean.substring(0, 16);
      }

      String val = clean.replaceAllMapped(
        new RegExp(r"\d{4}"),
        (Match m) => "${m[0]}$separator",
      );

      if (val.endsWith(" ")) {
        val = val.substring(0, val.length - 1);
      }

      return TextEditingValue(
        text: val,
        selection: TextSelection.collapsed(
          offset: val.length,
        ),
      );
    }
    return newValue;
  }
}
