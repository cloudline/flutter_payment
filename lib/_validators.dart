typedef String StringValidator(String value);

StringValidator defaultNumberValidator =
    (value) => value.length < 16 ? '' : null;

StringValidator defaultCVCValidator = (value) => value.length < 3 ? '' : null;

StringValidator defaultExpDateValidator =
    (value) => value.length < 5 ? '' : null;
