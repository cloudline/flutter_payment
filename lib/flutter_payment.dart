import 'dart:async';

import 'package:flutter/services.dart';

class Card {
  final String number;
  final int expYear;
  final int expMonth;
  final String cvc;

  String name;
  String addressCountry;
  String addressCity;
  String addressLine1;
  String addressLine2;
  String addressState;
  String addressZip;

  Card({
    this.number,
    this.expYear,
    this.expMonth,
    this.cvc,
    this.name,
    this.addressCountry,
    this.addressState,
    this.addressCity,
    this.addressLine1,
    this.addressLine2,
    this.addressZip,
  });

  factory Card.fromJson(Map<String, dynamic> json) => Card(
        number: json['number'] as String,
        expYear: json['exp_year'] as int,
        expMonth: json['exp_month'] as int,
        name: json['name'] as String,
        addressCity: json['address_city'] as String,
        addressCountry: json['address_country'] as String,
        addressLine1: json['address_line1'] as String,
        addressLine2: json['address_line2'] as String,
        addressState: json['address_state'] as String,
        addressZip: json['address_zip'] as String,
      );

  Map<String, dynamic> toJson() => {
        'number': number ?? '',
        'expYear': expYear ?? 0,
        'expMonth': expMonth ?? 0,
        'cvc': cvc ?? '',
        'name': name ?? '',
        'addressZip': addressZip ?? '',
        'addressLine1': addressLine1 ?? '',
        'addressLine2': addressLine2 ?? '',
        'addressCity': addressCity ?? '',
        'addressCountry': addressCountry ?? '',
        'addressState': addressState ?? '',
      };
}

class FlutterPayment {
  static const MethodChannel _channel = const MethodChannel('flutter_payment');

  static Future<String> get platformVersion async {
    return await _channel.invokeMethod('getPlatformVersion');
  }

  static Future<bool> validateCard(Card card) async {
    return await _channel.invokeMethod('validateCard', card.toJson());
  }

  static Future<bool> payWithWallet() async {
    return await _channel.invokeMethod('payWithWallet');
  }

  static Future<String> getCardToken(Card card) async {
    return await _channel.invokeMethod('getCreditCardToken', card.toJson());
  }

  static Future<bool> isWalletAvailable() async {
    try {
      return await _channel.invokeMethod('isWalletAvailable');
    } catch (e) {
      return false;
    }
  }
}
