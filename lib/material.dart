import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_payment/credit_card_formatter.dart';
import 'package:flutter_payment/exp_date_formatter.dart';
import 'package:flutter_payment/flutter_payment.dart' as stripe;

import '_validators.dart';

class CardForm extends StatefulWidget {
  final bool billingAddress;
  final bool holderName;
  final bool multiline;
  final bool address;
  final bool zipCode;
  final TextStyle textTheme;

  final StringValidator ccNumberValidator;
  final StringValidator ccCVCValidator;
  final StringValidator ccExpDateValidator;

  const CardForm({
    Key key,
    this.address = false,
    this.multiline = false,
    this.ccNumberValidator,
    this.ccCVCValidator,
    this.ccExpDateValidator,
    this.holderName = false,
    this.billingAddress = false,
    this.zipCode = false,
    this.textTheme,
  }) : super(key: key);

  @override
  CardFormState createState() => CardFormState();
}

class CardFormState extends State<CardForm> {
  GlobalKey<FormState> _formKey;
  TextEditingController _cardNumber;
  TextEditingController _cardExpDate;
  TextEditingController _cardCVC;
  TextEditingController _zipCode;
  StringValidator _ccNumberValidator;
  StringValidator _ccCVCValidator;
  StringValidator _ccExpDateValidator;

  String get cardNumber => _cardNumber.text;

  String get cardExpDate => _cardExpDate.text;

  String get cardCVC => _cardCVC.text;

  @override
  void dispose() {
    _cardNumber.dispose();
    _cardExpDate.dispose();
    _cardCVC.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _formKey = GlobalKey();
    _cardNumber = TextEditingController();
    _cardExpDate = TextEditingController();
    _cardCVC = TextEditingController();
    _zipCode = TextEditingController();

    _ccNumberValidator = widget.ccNumberValidator ?? defaultNumberValidator;
    _ccCVCValidator = widget.ccCVCValidator ?? defaultCVCValidator;
    _ccExpDateValidator = widget.ccExpDateValidator ?? defaultExpDateValidator;
    super.initState();
  }

  stripe.Card get card {
    final expMonth = int.parse(_cardExpDate.text.substring(0, 2));
    final expYear = int.parse(_cardExpDate.text.substring(3, 5));
    return stripe.Card(
      number: _cardNumber.text,
      expMonth: expMonth,
      expYear: expYear,
      cvc: _cardCVC.text,
      addressZip: _zipCode.text,
    );
  }

  bool validate() => _formKey.currentState.validate();

  void save() => _formKey.currentState.save();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: widget.zipCode ? formWithZipCode() : formWithoutZipCode(),
    );
  }

  Widget formWithoutZipCode() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      textBaseline: TextBaseline.alphabetic,
      children: <Widget>[
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 5, top: 10),
                child: Icon(
                  Icons.credit_card,
                  color: Colors.grey[400],
                ),
              ),
              Expanded(
                child: TextFormField(
                  controller: _cardNumber,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly,
                    BlacklistingTextInputFormatter.singleLineFormatter,
                    CreditCardFormatter(separator: ' ')
                  ],
                  style: widget.textTheme,
                  validator: _ccNumberValidator,
                  decoration: InputDecoration(
                    hintText: '4242 4242 4242 4242',
                    errorMaxLines: 1,
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          width: 70,
          margin: const EdgeInsets.symmetric(horizontal: 10),
          child: TextFormField(
            controller: _cardExpDate,
            keyboardType: TextInputType.number,
            validator: _ccExpDateValidator,
            textAlign: TextAlign.center,
            style: widget.textTheme,
            inputFormatters: [
              WhitelistingTextInputFormatter.digitsOnly,
              LengthLimitingTextInputFormatter(5),
              ExpDateFormatter(separator: '/'),
            ],
            decoration: InputDecoration(hintText: 'MM/YY', errorMaxLines: 1),
          ),
        ),
        Container(
          width: 50,
          child: TextFormField(
            controller: _cardCVC,
            keyboardType: TextInputType.number,
            validator: _ccCVCValidator,
            textAlign: TextAlign.center,
            style: widget.textTheme,
            inputFormatters: [
              WhitelistingTextInputFormatter.digitsOnly,
              LengthLimitingTextInputFormatter(3),
            ],
            decoration: InputDecoration(
              hintText: 'CVC',
              errorMaxLines: 1,
            ),
          ),
        ),
      ],
    );
  }

  Widget formWithZipCode() {
    return Column(
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          textBaseline: TextBaseline.alphabetic,
          children: <Widget>[
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 5, top: 10),
                    child: Icon(
                      Icons.credit_card,
                      color: Colors.grey[400],
                    ),
                  ),
                  Expanded(
                    child: TextFormField(
                      controller: _cardNumber,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        WhitelistingTextInputFormatter.digitsOnly,
                        BlacklistingTextInputFormatter.singleLineFormatter,
                        CreditCardFormatter(separator: ' ')
                      ],
                      style: widget.textTheme,
                      validator: _ccNumberValidator,
                      decoration: InputDecoration(
                        hintText: '4242 4242 4242 4242',
                        errorMaxLines: 1,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: 70,
              margin: const EdgeInsets.only(top: 8.0, left: 30),
              child: TextFormField(
                controller: _cardExpDate,
                keyboardType: TextInputType.number,
                validator: _ccExpDateValidator,
                textAlign: TextAlign.center,
                style: widget.textTheme,
                inputFormatters: [
                  WhitelistingTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(5),
                  ExpDateFormatter(separator: '/'),
                ],
                decoration:
                    InputDecoration(hintText: 'MM/YY', errorMaxLines: 1),
              ),
            ),
            Container(
              width: 50,
              margin: const EdgeInsets.only(top: 8.0),
              child: TextFormField(
                controller: _cardCVC,
                keyboardType: TextInputType.number,
                validator: _ccCVCValidator,
                textAlign: TextAlign.center,
                style: widget.textTheme,
                inputFormatters: [
                  WhitelistingTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(3),
                ],
                decoration: InputDecoration(
                  hintText: 'CVC',
                  errorMaxLines: 1,
                ),
              ),
            ),
            Container(
              width: 100,
              margin: const EdgeInsets.only(top: 8.0),
              child: TextFormField(
                controller: _zipCode,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.center,
                style: widget.textTheme,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(9),
                ],
                decoration: InputDecoration(
                  hintText: 'ZIP Code',
                  errorMaxLines: 1,
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
