import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_payment/credit_card_formatter.dart';
import 'package:flutter_payment/exp_date_formatter.dart';
import 'package:flutter_payment/flutter_payment.dart' as stripe;

class CardForm extends StatefulWidget {
  final bool billingAddress;
  final bool holderName;
  final bool autovalidate;
  final bool multiline;
  final bool address;

  const CardForm({
    Key key,
    this.address = false,
    this.multiline = false,
    this.autovalidate = false,
    this.holderName = false,
    this.billingAddress = false,
  }) : super(key: key);

  @override
  CardFormState createState() => CardFormState();
}

class CardFormState extends State<CardForm> {
  GlobalKey<FormState> _formKey;
  TextEditingController _cardNumber;
  TextEditingController _cardExpDate;
  TextEditingController _cardCVC;

  String get cardNumber => _cardNumber.text;

  String get cardExpDate => _cardExpDate.text;

  String get cardCVC => _cardCVC.text;

  @override
  void initState() {
    _formKey = GlobalKey();
    _cardNumber = TextEditingController();
    _cardExpDate = TextEditingController();
    _cardCVC = TextEditingController();
    super.initState();
  }

  stripe.Card get card {
    final expDate = _cardExpDate.text;
    final expMonth = int.parse(expDate.substring(0, 2));
    final expYear = int.parse(expDate.substring(3, 5));
    return stripe.Card(
      number: _cardNumber.text,
      expMonth: expMonth,
      expYear: expYear,
      cvc: _cardCVC.text,
    );
  }

  bool validate() => _formKey.currentState.validate();

  void save() => _formKey.currentState.save();

  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidate: widget.autovalidate,
      key: _formKey,
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(16.0),
              child: CupertinoTextField(
                controller: _cardNumber,
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly,
                  BlacklistingTextInputFormatter.singleLineFormatter,
                  CreditCardFormatter(separator: ' ')
                ],
                style: TextStyle(fontSize: 22),
                placeholder: 'Credit Card Number',
              ),
            ),
            Container(
              margin: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: CupertinoTextField(
                      controller: _cardExpDate,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(5),
                        ExpDateFormatter(separator: '/'),
                      ],
                      placeholder: 'Exp. Date',
                    ),
                  ),
                  Spacer(),
                  Flexible(
                    child: CupertinoTextField(
                      controller: _cardCVC,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(3),
                      ],
                      placeholder: 'CVC',
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
