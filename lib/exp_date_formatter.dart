import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ExpDateFormatter extends TextInputFormatter {
  final String separator;

  ExpDateFormatter({
    @required this.separator,
  }) {
    assert(separator != null);
  }

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length > 0) {
      String clean = newValue.text.replaceAll(new RegExp(separator), '');
      String val = clean.replaceAllMapped(
        new RegExp(r"\d{2}"),
        (Match m) => "${m[0]}$separator",
      );

      if (val.endsWith(separator)) {
        val = val.substring(0, val.length - 1);
      }

      // Check that the month starts with a value under 1
      if (val.length == 1 && int.parse(val) > 1) {
        return TextEditingValue(
          text: oldValue.text,
          selection: TextSelection.collapsed(offset: oldValue.text.length),
        );
      }

      // check that the full month is not greater than 12
      if (val.length <= 2 && int.parse(val) > 12) {
        return TextEditingValue(
          text: oldValue.text,
          selection: TextSelection.collapsed(offset: oldValue.text.length),
        );
      }

      // check if the year is not lower than current year
      if (val.length == 5) {
        final parts = val.split(separator);
        final intVal = int.parse(parts[1]);
        final currentVal =
            int.parse(DateTime.now().year.toString().substring(2, 4));

        if (intVal < currentVal) {
          return TextEditingValue(
            text: oldValue.text,
            selection: TextSelection.collapsed(offset: oldValue.text.length),
          );
        }
      }

      return TextEditingValue(
        text: val,
        selection: TextSelection.collapsed(offset: val.length),
      );
    }
    return newValue;
  }
}
