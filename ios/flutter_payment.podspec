#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
Pod::Spec.new do |s|
  s.name             = 'flutter_payment'
  s.version          = '0.0.5'
  s.summary          = 'A Stripe plugin for Flutter'
  s.description      = <<-DESC
A Stripe plugin for Flutter
                       DESC
  s.homepage         = 'http://cloudline-solutions.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Cloudline Solutions' => 'nico@cloudline-solutions.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.dependency 'Stripe', '17.0.2'
  s.ios.deployment_target = '9.0'
end

