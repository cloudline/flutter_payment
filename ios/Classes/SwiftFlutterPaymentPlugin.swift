import Flutter
import UIKit
import Stripe

public class SwiftFlutterPaymentPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    guard let publishableKey = plistValues(bundle: Bundle.main) else { return }
    STPPaymentConfiguration.shared().publishableKey = publishableKey
    
    let channel = FlutterMethodChannel(name: "flutter_payment", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterPaymentPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch call.method {
    case "getPlatformVersion":
        result("iOS " + UIDevice.current.systemVersion)
    case "validateCard":
        result(true)
    case "getCreditCardToken":
        guard let args = call.arguments else {
            result(FlutterError(code: "INVALID_ARGUMENTS", message: "can't retrieve arguments", details: nil))
            return
        }
        
        if let myArgs = args as? [String: Any],
            let number = myArgs["number"] as? String,
            let expMonth = myArgs["expMonth"] as? UInt,
            let expYear = myArgs["expYear"] as? UInt,
            let cvc = myArgs["cvc"] as? String {
            
            let card = STPCardParams()
            card.number = number
            card.expYear = expYear
            card.expMonth = expMonth
            card.cvc = cvc
            
            STPAPIClient.shared().createToken(withCard: card) { (token: STPToken?, error: Error?) in
                guard let mToken = token, error == nil else {
                    result(FlutterError(code: "INVALID_TOKEN", message: error?.localizedDescription, details: nil))
                    return
                }
                result(mToken.stripeID)
            }
        } else {
            result(FlutterError(code: "INVALID_ARGUMENTS", message: "iOS could not extract flutter arguments in method: (sendParams)", details: nil))
        }
    default:
        result(FlutterError(code: "NOT_IMPLEMENTED", message: "not implemented", details: nil))
    }
  }
}

func plistValues(bundle: Bundle) -> String? {
    guard
        let path = bundle.path(forResource: "Stripe", ofType: "plist"),
        let values = NSDictionary(contentsOfFile: path) as? [String: Any]
        else {
            print("Missing Stripe.plist file with 'PublishableKey' entry in main bundle!")
            return nil
    }
    
    guard
        let publishableKey = values["PublishableKey"] as? String
        else {
            print("Stripe.plist file at \(path) is missing 'PublishableKey' entry!")
            print("File currently has the following entries: \(values)")
            return nil
    }
    return publishableKey
}
